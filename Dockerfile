# Base image https://hub.docker.com/u/rocker/
FROM rocker/shiny-verse:4.0.3
LABEL author="David Martínez david.martinez@macer.tech"

COPY . .

RUN Rscript -e "install.packages(c('rhino'), repos='http://cran.rstudio.com')"
# Expose port
EXPOSE 4040

# run app on container start
CMD ["R", "-e", "shiny::runApp('/app', host = '0.0.0.0', port = 4040)"]